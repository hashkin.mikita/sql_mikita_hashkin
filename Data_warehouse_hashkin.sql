-- Creating staging tables for loading initial data

CREATE TABLE staging_customers (
    customer_id varchar(255) PRIMARY KEY,
    company_name varchar(255),
    contact_name varchar(255),
    contact_title varchar(255),
    address varchar(255),
    city varchar(255),
    region varchar(255),
    postal_code varchar(255),
    country varchar(255),
    phone varchar(255),
    fax varchar(255)
);

CREATE TABLE staging_orders (
    order_id int PRIMARY KEY,
    customer_id varchar(255),
    employee_id int,
    order_date date,
    required_date date,
    shipped_date date,
    shipper_id int,
    freight decimal(18, 2),
    ship_name varchar(255),
    ship_address varchar(255),
    ship_city varchar(255),
    ship_region varchar(255),
    ship_postal_code varchar(255),
    ship_country varchar(255)
);

CREATE TABLE staging_order_details (
    order_id int,
    product_id int,
    unit_price decimal(18, 2),
    quantity smallint,
    discount real,
    PRIMARY KEY (order_id, product_id)
);

CREATE TABLE staging_products (
    product_id int PRIMARY KEY,
    product_name varchar(255),
    supplier_id int,
    category_id int,
    quantity_per_unit varchar(255),
    unit_price decimal(18, 2),
    units_in_stock smallint,
    units_on_order smallint,
    reorder_level smallint,
    discontinued bit
);

CREATE TABLE staging_employees (
    employee_id int PRIMARY KEY,
    last_name varchar(255),
    first_name varchar(255),
    title varchar(255),
    title_of_courtesy varchar(255),
    birth_date date,
    hire_date date,
    address varchar(255),
    city varchar(255),
    region varchar(255),
    postal_code varchar(255),
    country varchar(255),
    home_phone varchar(255),
    extension varchar(255),
    notes text,
    manager_id int,
    photo_path varchar(255)
);

CREATE TABLE staging_categories (
    category_id int PRIMARY KEY,
    category_name varchar(255),
    description text,
    picture bytea
);

CREATE TABLE staging_shippers (
    shipper_id int PRIMARY KEY,
    company_name varchar(255),
    phone varchar(255)
);

CREATE TABLE staging_suppliers (
    supplier_id int PRIMARY KEY,
    company_name varchar(255),
    contact_name varchar(255),
    contact_title varchar(255),
    address varchar(255),
    city varchar(255),
    region varchar(255),
    postal_code varchar(255),
    country varchar(255),
    phone varchar(255),
    fax varchar(255),
    homepage text
);



-- Creation of dimension tables for the star schema

CREATE TABLE DimDate (
    DateID int PRIMARY KEY,
    Date date,
    Day int,
    Month int,
    Year int,
    Quarter int,
    WeekOfYear int
);

CREATE TABLE DimCustomer (
    CustomerID varchar(255) PRIMARY KEY,
    CompanyName varchar(255),
    ContactName varchar(255),
    ContactTitle varchar(255),
    Address varchar(255),
    City varchar(255),
    Region varchar(255),
    PostalCode varchar(255),
    Country varchar(255),
    Phone varchar(255)
);

CREATE TABLE DimProduct (
    ProductID int PRIMARY KEY,
    ProductName varchar(255),
    SupplierID int,
    CategoryID int,
    QuantityPerUnit varchar(255),
    UnitPrice decimal(18, 2),
    UnitsInStock smallint
);

CREATE TABLE DimEmployee (
    EmployeeID int PRIMARY KEY,
    LastName varchar(255),
    FirstName varchar(255),
    Title varchar(255),
    BirthDate date,
    HireDate date,
    Address varchar(255),
    City varchar(255),
    Region varchar(255),
    PostalCode varchar(255),
    Country varchar(255),
    HomePhone varchar(255),
    Extension varchar(255)
);

CREATE TABLE DimCategory (
    CategoryID int PRIMARY KEY,
    CategoryName varchar(255),
    Description text
);

CREATE TABLE DimShipper (
    ShipperID int PRIMARY KEY,
    CompanyName varchar(255),
    Phone varchar(255)
);

CREATE TABLE DimSupplier (
    SupplierID int PRIMARY KEY,
    CompanyName varchar(255),
    ContactName varchar(255),
    ContactTitle varchar(255),
    Address varchar(255),
    City varchar(255),
    Region varchar(255),
    PostalCode varchar(255),
    Country varchar(255),
    Phone varchar(255)
);


-- Loading data from staging tables into dimension tables

INSERT INTO DimCustomer (CustomerID, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone)
SELECT customer_id, company_name, contact_name, contact_title, address, city, region, postal_code, country, phone
FROM staging_customers;

INSERT INTO DimProduct (ProductID, ProductName, SupplierID, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock)
SELECT product_id, product_name, supplier_id, category_id, quantity_per_unit, unit_price, units_in_stock
FROM staging_products;

INSERT INTO DimEmployee (EmployeeID, LastName, FirstName, Title, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Extension)
SELECT employee_id, last_name, first_name, title, birth_date, hire_date, address, city, region, postal_code, country, home_phone, extension
FROM staging_employees;

INSERT INTO DimCategory (CategoryID, CategoryName, Description)
SELECT category_id, category_name, description
FROM staging_categories;

INSERT INTO DimShipper (ShipperID, CompanyName, Phone)
SELECT shipper_id, company_name, phone
FROM staging_shippers;

INSERT INTO DimSupplier (SupplierID, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone)
SELECT supplier_id, company_name, contact_name, contact_title, address, city, region, postal_code, country, phone
FROM staging_suppliers;

INSERT INTO DimCustomer (CustomerID, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone)
SELECT customer_id, company_name, contact_name, contact_title, address, city, region, postal_code, country, phone
FROM staging_customers;

INSERT INTO DimProduct (ProductID, ProductName, SupplierID, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock)
SELECT product_id, product_name, supplier_id, category_id, quantity_per_unit, unit_price, units_in_stock
FROM staging_products;

INSERT INTO DimEmployee (EmployeeID, LastName, FirstName, Title, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Extension)
SELECT employee_id, last_name, first_name, title, birth_date, hire_date, address, city, region, postal_code, country, home_phone, extension
FROM staging_employees;

INSERT INTO DimCategory (CategoryID, CategoryName, Description)
SELECT category_id, category_name, description
FROM staging_categories;

INSERT INTO DimShipper (ShipperID, CompanyName, Phone)
SELECT shipper_id, company_name, phone
FROM staging_shippers;

INSERT INTO DimSupplier (SupplierID, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone)
SELECT supplier_id, company_name, contact_name, contact_title, address, city, region, postal_code, country, phone
FROM staging_suppliers;


-- Creating a fact table for a star schema

CREATE TABLE FactSales (
    SalesID int PRIMARY KEY,
    DateID int,
    CustomerID varchar(255),
    ProductID int,
    EmployeeID int,
    CategoryID int,
    ShipperID int,
    SupplierID int,
    QuantitySold smallint,
    UnitPrice decimal(18, 2),
    Discount real,
    TotalAmount decimal(18, 2),
    TaxAmount decimal(18, 2),
    FOREIGN KEY (DateID) REFERENCES DimDate(DateID),
    FOREIGN KEY (CustomerID) REFERENCES DimCustomer(CustomerID),
    FOREIGN KEY (ProductID) REFERENCES DimProduct(ProductID),
    FOREIGN KEY (EmployeeID) REFERENCES DimEmployee(EmployeeID),
    FOREIGN KEY (CategoryID) REFERENCES DimCategory(CategoryID),
    FOREIGN KEY (ShipperID) REFERENCES DimShipper(ShipperID),
    FOREIGN KEY (SupplierID) REFERENCES DimSupplier(SupplierID)
);

-- Sales for the first week of each month
SELECT 
    Month,
    SUM(TotalAmount) AS TotalSalesAmount,
    SUM(QuantitySold) AS TotalQuantitySold
FROM 
    FactSales
JOIN 
    DimDate ON FactSales.DateID = DimDate.DateID
WHERE 
    Day BETWEEN 1 AND 7
GROUP BY 
    Month
ORDER BY 
    Month;

