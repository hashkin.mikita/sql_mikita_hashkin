-- This script modifies the DimEmployee table to support SCD Type 2.

-- Adding necessary columns for SCD Type 2
ALTER TABLE DimEmployee
ADD COLUMN effective_start TIMESTAMP;

ALTER TABLE DimEmployee
ADD COLUMN effective_end TIMESTAMP;

ALTER TABLE DimEmployee
ADD COLUMN is_current BOOLEAN DEFAULT TRUE;

-- Dropping the existing primary key constraint
ALTER TABLE DimEmployee
DROP CONSTRAINT DimEmployee_pkey;

-- Adding a new surrogate key column
ALTER TABLE DimEmployee
ADD COLUMN employee_history_id SERIAL PRIMARY KEY;

-- Updating the new primary key column with default values
UPDATE DimEmployee
SET employee_history_id = DEFAULT;

-- Setting effective start and end dates for existing records
UPDATE DimEmployee
SET effective_start = HireDate,
    effective_end = '7777-07-07';


-- This script creates a function and trigger to handle SCD Type 2 updates for the DimEmployee table.

-- Creating the function to manage updates for SCD Type 2
CREATE OR REPLACE FUNCTION handle_dim_employee_updates()
RETURNS TRIGGER AS $$
BEGIN
    -- Check if there are changes in the Title or Address columns
    IF (OLD.Title <> NEW.Title OR OLD.Address <> NEW.Address) AND OLD.is_current AND NEW.is_current THEN
        -- Updating the current record to set effective_end and is_current
        UPDATE DimEmployee
        SET effective_end = current_timestamp,
            is_current = FALSE
        WHERE EmployeeID = OLD.EmployeeID AND is_current = TRUE;

        -- Inserting a new record with the updated information
        INSERT INTO DimEmployee (EmployeeID, LastName, FirstName, Title, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Extension, effective_start, effective_end, is_current)
        VALUES (OLD.EmployeeID, OLD.LastName, OLD.FirstName, NEW.Title, OLD.BirthDate, OLD.HireDate, NEW.Address, OLD.City, OLD.Region, OLD.PostalCode, OLD.Country, OLD.HomePhone, OLD.Extension, current_timestamp, '9999-12-31', TRUE);
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Creating the trigger to call the function after updates
CREATE TRIGGER update_dim_employee_trigger
AFTER UPDATE ON DimEmployee
FOR EACH ROW
EXECUTE FUNCTION handle_dim_employee_updates();


-- This script tests the SCD Type 2 implementation by updating some records in the DimEmployee table.

-- Updating an employee's address to test SCD Type 2 functionality
UPDATE DimEmployee
SET Address = 'Paris'
WHERE FirstName = 'Nancy' AND LastName = 'Davolio' AND is_current = TRUE;

-- Updating an employee's title to test SCD Type 2 functionality
UPDATE DimEmployee
SET Title = 'Senior Sales Representative'
WHERE FirstName = 'Nancy' AND LastName = 'Davolio' AND is_current = TRUE;
