-- Create Fact Table: FactSupplierPurchases
-- This table will store aggregated purchase data per supplier
CREATE TABLE IF NOT EXISTS FactSupplierPurchases (
    PurchaseID SERIAL PRIMARY KEY,
    SupplierID INT,
    TotalPurchaseAmount DECIMAL,
    PurchaseDate DATE,
    NumberOfProducts INT,
    FOREIGN KEY (SupplierID) REFERENCES DimSupplier(SupplierID)
);

-- Populate FactSupplierPurchases table with aggregated data
-- Aggregating data from staging_order_details and staging_products to calculate total purchase amount and number of products per supplier
INSERT INTO FactSupplierPurchases (SupplierID, TotalPurchaseAmount, PurchaseDate, NumberOfProducts)
SELECT 
    p.SupplierID, 
    SUM(od.unit_price * od.quantity) AS TotalPurchaseAmount, 
    CURRENT_DATE AS PurchaseDate, 
    COUNT(DISTINCT od.product_id) AS NumberOfProducts
FROM staging_order_details od
JOIN staging_products p ON od.product_id = p.product_id
GROUP BY p.SupplierID;

-- Create Fact Table: FactProductSales
-- This table will store sales data per product
CREATE TABLE IF NOT EXISTS FactProductSales (
    FactSalesID SERIAL PRIMARY KEY,
    DateID INT,
    ProductID INT,
    QuantitySold INT,
    TotalSales DECIMAL(10,2),
    FOREIGN KEY (DateID) REFERENCES DimDate(DateID),
    FOREIGN KEY (ProductID) REFERENCES DimProduct(ProductID)
);

-- Populate FactProductSales table with aggregated data
-- Aggregating data from staging_order_details and staging_orders to calculate total sales per product
INSERT INTO FactProductSales (DateID, ProductID, QuantitySold, TotalSales)
SELECT 
    d.DateID,
    p.product_id, 
    od.quantity, 
    (od.quantity * od.unit_price) AS TotalSales
FROM staging_order_details od
JOIN staging_orders s ON od.order_id = s.order_id
JOIN dimdate d ON s.orderdate = d.Date
JOIN staging_products p ON od.product_id = p.product_id;

-- Supplier Spending Analysis
-- Calculate total spend per supplier grouped by year and month
SELECT
    s.CompanyName,
    SUM(fsp.TotalPurchaseAmount) AS TotalSpend,
    EXTRACT(YEAR FROM fsp.PurchaseDate) AS Year,
    EXTRACT(MONTH FROM fsp.PurchaseDate) AS Month
FROM FactSupplierPurchases fsp
JOIN DimSupplier s ON fsp.SupplierID = s.SupplierID
GROUP BY s.CompanyName, Year, Month
ORDER BY TotalSpend DESC;

-- Product Cost Breakdown by Supplier
-- Calculate average unit price, total quantity purchased, and total spend per supplier and product
SELECT
    s.CompanyName,
    p.ProductName,
    AVG(od.unit_price) AS AverageUnitPrice,
    SUM(od.quantity) AS TotalQuantityPurchased,
    SUM(od.unit_price * od.quantity) AS TotalSpend
FROM staging_order_details od
JOIN staging_products p ON od.product_id = p.product_id
JOIN DimSupplier s ON p.SupplierID = s.SupplierID
GROUP BY s.CompanyName, p.ProductName
ORDER BY s.CompanyName, TotalSpend DESC;

-- Top Five Products by Total Purchases per Supplier
-- Calculate top 5 products by total spend per supplier
SELECT
    s.CompanyName,
    p.ProductName,
    SUM(od.unit_price * od.quantity) AS TotalSpend
FROM staging_order_details od
JOIN staging_products p ON od.product_id = p.product_id
JOIN DimSupplier s ON p.SupplierID = s.SupplierID
GROUP BY s.CompanyName, p.ProductName
ORDER BY s.CompanyName, TotalSpend DESC
LIMIT 5;

-- Top-Selling Products
-- Calculate total quantity sold and total revenue per product
SELECT 
    p.ProductName,
    SUM(fps.QuantitySold) AS TotalQuantitySold,
    SUM(fps.TotalSales) AS TotalRevenue
FROM 
    FactProductSales fps
JOIN DimProduct p ON fps.ProductID = p.ProductID
GROUP BY p.ProductName
ORDER BY TotalRevenue DESC
LIMIT 5;

-- Sales Trends by Product Category
-- Calculate total quantity sold and total revenue per product category grouped by year and month
SELECT 
    c.CategoryName, 
    EXTRACT(YEAR FROM d.Date) AS Year,
    EXTRACT(MONTH FROM d.Date) AS Month,
    SUM(fps.QuantitySold) AS TotalQuantitySold,
    SUM(fps.TotalSales) AS TotalRevenue
FROM 
    FactProductSales fps
JOIN DimProduct p ON fps.ProductID = p.ProductID
JOIN DimCategory c ON p.CategoryID = c.CategoryID
JOIN DimDate d ON fps.DateID = d.DateID
GROUP BY c.CategoryName, Year, Month
ORDER BY Year, Month, TotalRevenue DESC;

-- Inventory Valuation
-- Calculate inventory value for each product
SELECT 
    p.ProductName,
    p.UnitsInStock,
    p.UnitPrice,
    (p.UnitsInStock * p.UnitPrice) AS InventoryValue
FROM 
    DimProduct p
ORDER BY InventoryValue DESC;

-- Supplier Performance Based on Product Sales
-- Calculate the number of sales transactions, total products sold, and total revenue generated per supplier
SELECT 
    s.CompanyName,
    COUNT(DISTINCT fps.FactSalesID) AS NumberOfSalesTransactions,
    SUM(fps.QuantitySold) AS TotalProductsSold,
    SUM(fps.TotalSales) AS TotalRevenueGenerated
FROM 
    FactProductSales fps
JOIN DimProduct p ON fps.ProductID = p.ProductID
JOIN DimSupplier s ON p.SupplierID = s.SupplierID
GROUP BY s.CompanyName
ORDER BY TotalRevenueGenerated DESC;

-- Aggregate Sales by Month and Category
-- Calculate total sales per category grouped by month and year
SELECT d.Month, d.Year, c.CategoryName, SUM(fs.TotalAmount) AS TotalSales
FROM FactSales fs
JOIN DimDate d ON fs.DateID = d.DateID
JOIN DimCategory c ON fs.CategoryID = c.CategoryID
GROUP BY d.Month, d.Year, c.CategoryName
ORDER BY d.Year, d.Month, TotalSales DESC;

-- Top-Selling Products per Quarter
-- Calculate total quantity sold per product grouped by quarter and year
SELECT d.Quarter, d.Year, p.ProductName, SUM(fs.QuantitySold) AS TotalQuantitySold
FROM FactSales fs
JOIN DimDate d ON fs.DateID = d.DateID
JOIN DimProduct p ON fs.ProductID = p.ProductID
GROUP BY d.Quarter, d.Year, p.ProductName
ORDER BY d.Year, d.Quarter, TotalQuantitySold DESC
LIMIT 5;

-- Customer Sales Overview
-- Calculate total spent and number of transactions per customer
SELECT cu.CompanyName, SUM(fs.TotalAmount) AS TotalSpent, COUNT(DISTINCT fs.SalesID) AS TransactionsCount
FROM FactSales fs
JOIN DimCustomer cu ON fs.CustomerID = cu.CustomerID
GROUP BY cu.CompanyName
ORDER BY TotalSpent DESC;

-- Sales Performance by Employee
-- Calculate number of sales and total sales amount per employee
SELECT e.FirstName, e.LastName, COUNT(fs.SalesID) AS NumberOfSales, SUM(fs.TotalAmount) AS TotalSales
FROM FactSales fs
JOIN DimEmployee e ON fs.EmployeeID = e.EmployeeID
GROUP BY e.FirstName, e.LastName
ORDER BY TotalSales DESC;

-- Monthly Sales Growth Rate
-- Calculate the growth rate of total sales per month
WITH MonthlySales AS (
    SELECT
        d.Year,
        d.Month,
        SUM(fs.TotalAmount) AS TotalSales
    FROM FactSales fs
    JOIN DimDate d ON fs.DateID = d.DateID
    GROUP BY d.Year, d.Month
),
MonthlyGrowth AS (
    SELECT
        Year,
        Month,
        TotalSales,
        LAG(TotalSales) OVER (ORDER BY Year, Month) AS PreviousMonthSales,
        (TotalSales - LAG(TotalSales) OVER (ORDER BY Year, Month)) / LAG(TotalSales) OVER (ORDER BY Year, Month) AS GrowthRate
    FROM MonthlySales
)
SELECT * FROM MonthlyGrowth;
